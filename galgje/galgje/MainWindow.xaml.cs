﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace galgje
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string geheimWoord;
        string geraden;
        string juistenLetters;
        string fouteLetters;

        string[] verborgenLetters;

        bool letterJuist;

        int levens = 10;

        public MainWindow()
        {
           
            InitializeComponent();

            btnRaad.Visibility = Visibility.Hidden;
        }

        
        private void btnNieuwSpel_Click(object sender, RoutedEventArgs e)
        {
            //begin requarments
            btnVerbergWoord.IsEnabled = true;
            txtIngeefveld.IsEnabled = true;
            lblInfo.Content = $"Geef een geheim \nwoord";

            //reset requarments
            btnRaad.Visibility = Visibility.Hidden;
            geheimWoord = "";
            btnVerbergWoord.Visibility = Visibility.Visible;
            levens = 10;

        }

        private void btnVerbergWoord_Click(object sender, RoutedEventArgs e)
        {
            geheimWoord = txtIngeefveld.Text;
            txtIngeefveld.Text = "";
            btnVerbergWoord.Visibility = Visibility.Hidden;
            btnRaad.Visibility = Visibility.Visible;

            verborgenLetters = new string[geheimWoord.Length];

            for (int i = 0; i < geheimWoord.Length; i++)
            {
                verborgenLetters[i] = "_";
            }



            lblInfo.Content = $"Raad de letters of\n" +
                              $"het een woord\n" +
                              $"je hebt {levens} levens\n" +
                              $"{verborgenLetters}";
        }

        private void btnRaad_Click(object sender, RoutedEventArgs e)
        {
            geraden = txtIngeefveld.Text;

           //word er een woord ingegeven,letter of niks?
                //niks igegeven
            if(geraden.Length==0)
            {
                //er gebeurt niks 
                lblInfo.Content = $"je hebt niks ingegeven\n" +
                                  $"{levens} levens\n"+
                                  $"Juiste letters: {juistenLetters} \n" +
                                  $"Foute letters: {fouteLetters}"; ;
            }
                //letter ingegeven
            else if (geraden.Length == 1)
            {
                //letters nalezen
                for (int i = 0; i < geheimWoord.Length; i++)
                {
                    string letter = Convert.ToString(geheimWoord[i]);

                    //letter is juist 
                    if (geraden.Contains(letter))
                    {
                        letterJuist = true;
                        
                    }
                    //letter is fout 
                    else
                    {
                        letterJuist = false;                         
                      
                    }
                }
                //uitvoering juist fout letter
                if (letterJuist == true)
                {
                    juistenLetters += $" {geraden}";

                    lblInfo.Content = $"Letter Juist geraden\n{levens} levens \n" +
                                  $"Juiste letters: {juistenLetters} \n" +
                                  $"Foute letters: {fouteLetters}";

                    txtIngeefveld.Text = "";
                }
                else
                {
                    fouteLetters += $" {geraden}"; 
                    
                    levens--;

                    lblInfo.Content = $"Letter fout geraden\n{levens} levens \n" +
                                  $"Juiste letters: {juistenLetters} \n" +
                                  $"Foute letters: {fouteLetters}";
                    txtIngeefveld.Text = "";

                }
                                    
                
            }
                //woord ingegeven wanr lenght > 1
            else
            {

                    //woord geraden
                if (geraden == geheimWoord)
                {
                    btnRaad.IsEnabled = false;
                    txtIngeefveld.IsEnabled = false;


                    lblInfo.Content = $"Proficiat \n" +
                                      $"Spel gewonnen \n" +
                                      $"Het woord was \n" +
                                      $"{geheimWoord}";

                    txtIngeefveld.Text = "";

                }
                    //woord niet geraden
                else if(geraden != geheimWoord)
                {
                    levens--;
                        //tekst
                    lblInfo.Content = $"Woord fout geraden\n{levens} levens \n" +
                                      $"Juiste letters: {juistenLetters} \n" +
                                      $"Foute letters: {fouteLetters}";

                    txtIngeefveld.Text = "";
                }


            }
            //game over verloren 
            if( levens == 0)
            {
                lblInfo.Content = $"Game over\n" +
                                  $"Woord niet geraden\n" +
                                  $"Het woord was {geheimWoord}";

                
            }




            //-----------------------------------------------------









        }
    }
}
